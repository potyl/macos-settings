SHELL := bash

COLOR_START     = \e[91m\e[1m
COLOR_END       = \e[0m
SAY             = @printf "$(COLOR_START)%s\n$(COLOR_END)"


##
## Ad hoc commands
##

.PHONY: dump
dump:
	defaults read > defaults.txt
