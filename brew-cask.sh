#!/usr/bin/env bash

set -euo pipefail

# Show an important message that needs to standout
function info {
    printf "\e[32;1m"
    echo -n "$@"
    printf "\e[m\n"
}

function cask {
  local app="$1"
  info "Installing ${app}"

  # Check if the app is already installed
  local macos_app=$(brew info --json=v2 --cask "${app}" | jq -r '.casks[].artifacts[].app | select(type == "array") | .[]')
  if [[ -d "/Applications/${macos_app}" ]]; then
    echo "${app} is already installed"
    echo
    return
  fi

  # Install the app
  brew install --cask "${app}"
  echo
}

# Install apps
#cask bitwarden
cask brave-browser
cask brave-browser@beta
cask docker
cask ghostty
cask hammerspoon
cask slack
cask spotify
cask spotify
cask visual-studio-code
cask whatsapp
cask wireshark

exit 0
