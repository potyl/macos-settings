#!/usr/bin/env bash

set -euo pipefail

# Install software
brew install aspell
brew install awscli
brew install bash
brew install coreutils
brew install dialog
brew install direnv
brew install dive
brew install fnm
brew install gh
brew install git
brew install git-gui
brew install go
brew install helm
brew install htop
brew install java
brew install jo
brew install jq
brew install k6
brew install kubectx
brew install kubernetes-cli
brew install libpq # postres client
brew install make
brew install mkcert
brew install mysql-client
brew install nano
brew install nanorc
brew install terraform
brew install tkdiff
brew install tree
brew install wget
brew install yq

exit 0
