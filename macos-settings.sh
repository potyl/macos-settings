#!/usr/bin/env bash

set -euo pipefail

# Announce something with color
function say {
    printf "\e[34;1m"
    echo -n "$@"
    printf "\e[m\n"
}



####
## Sound
####

say "Sound settings"

# Disable the sound effects on boot
sudo nvram SystemAudioVolume=' '


####
## Appearance
###

say "Appearance settings"

# Disable transparency in the menu bar and elsewhere
sudo defaults write com.apple.universalaccess reduceTransparency -bool true


##
## End
##

exit 0
